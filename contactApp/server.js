var express=require("express");
var app=express();
//app.get('/', function(req, res){
//res.send("hello world from service js");
//});

var mongojs=require("mongojs");
var db=mongojs('contactList', ['contactList']);
var bodyParser=require('body-parser');

app.use(express.static(__dirname + '/public'))
app.use(bodyParser.json())
app.get('/contactlist', function(req, res){
	console.log('i got request from clint side for data');
	
/* 	var person1= {
		name: 'Niraj kumar',
		email : 'niraj@gmail.com',
		mobileNo : '8553051910'
		
	};
	var person2= {
		name: 'Dheeraj kumar',
		email : 'dheeraj@gmail.com',
		mobileNo : '8553051911'
		
	};
	var person3= {
		name: 'Pankaj kumar',
		email : 'pankaj@gmail.com',
		mobileNo : '8553051912'
		
	};
	var contactList=[person1,person2,person3];
	
	res.json(contactList); */
	
	//instead of above code now we are going to take all data from data base
	
	db.contactList.find(function(erro, doc){
		console.log(doc);
		res.json(doc);
	});

});

app.post('/contactlist', function(req, res){
	console.log(req.body);
	db.contactList.insert(req.body, function(erro, doc){
		res.json(doc);
	});
});

app.delete('/contactlist/:id', function(req, res){
	var id=req.params.id;
	db.contactList.remove({_id : mongojs.ObjectId(id)}, function(err, doc){
     res.json(doc);
	});
});

app.get('/contactlist/:id', function(req, res){
	var id=req.params.id;
	console.log(id);
	db.contactList.findOne({_id : mongojs.ObjectId(id)}, function(err, doc){
     res.json(doc);
	});

});

app.put('/contactlist/:id', function(req, res){
	var id=req.params.id;
   console.log(req.body)
  db.contactList.findAndModify({query : {_id : mongojs.ObjectId(id)}, update :{$set:{name: req.body.name, email: req.body.email, mobileNo : req.body.mobileNo}}, new: true}, function(err, doc){
  	res.json(doc);
})
});

app.listen(3000);
console.log("server is running on port no 3000");